﻿using System.Collections.Generic;
using System.Linq;

namespace InterviewAssessment
{
    public class Scenarios
    {
        /// <summary>
        /// Implement this method.
        /// </summary>
        public string Scenario1(string str) {
            string answer = "";

            for (int i = str.Length - 1; i >= 0; i--) {
                answer += str[i];
            }
         
            return answer;
        }


        /// <summary>
        /// Fix this method.
        /// </summary>
        public int Scenario2(int @base, int exponent)
        {
            int n = 1;

            for (int i = 1; i <= exponent; i++)
            {
                n *= @base;
            }

            return n;
        }

        /// <summary>
        /// DO NOT MODIFY
        /// </summary>
        public string Scenario3<T>(T obj) => typeof(T).Name;

        /// <summary>
        /// DO NOT MODIFY
        /// </summary>
        public string Scenario3(string str) => str;

        /// <summary>
        /// Implement this method.
        /// </summary>
        public string Scenario4(Node node) {

            if (node.GetType() == typeof(Tree))
            {
                var tree = (Tree)node;
                return GetTreeText(tree);
            } 
            else
            {
                return node.Text;
            }
        }

        private string GetTreeText(Tree tree)
        {
            string answer = tree.Text;

            for (int i = 0; i < tree.Children.Count(); i++)
            {
                var childElement = tree.Children.ElementAt(i);

                if (childElement.GetType() == typeof(Tree))
                {
                    var childTree = (Tree)childElement;

                    answer += "-" + Scenario4((Tree)childTree);
                }
                else
                {
                    answer += "-" + tree.Children.ElementAt(i).Text;
                }
            }

            return answer;
        }
    }

    public class Node
    {
        public Node(string text)
        {
            Text = text;
        }

        public string Text { get; set; }
    }

    public class Tree : Node
    {
        public Tree(string text, params Node[] children) : base(text)
        {
            Children = children;
        }

        public IEnumerable<Node> Children { get; set; }
    }
}
